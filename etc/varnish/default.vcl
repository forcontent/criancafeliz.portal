# VCL configuration file for Varnish.
# It defines backends for each Plone instance and uses hash load balancing.
#
# This file must me symlinked to /etc/varnish/default.vcl

vcl 4.0;

import directors;
import std;

# probe definition for health checks: 3 of the last 5 tests must succeed

probe healthcheck {
    .interval = 10s;
    .request = "HEAD / HTTP/1.1";
    .timeout = 2s;
    .threshold = 3;
    .window = 5;
}

# backend definitions for Plone instances

backend intance1 {
    .host = "127.0.0.1"; .port = "8081"; .probe = healthcheck;
}

backend intance2 {
    .host = "127.0.0.1"; .port = "8082"; .probe = healthcheck;
}

acl purge {
    "127.0.0.1";
}

sub vcl_init {
    if (req.method == "PURGE") {
        if (!client.ip ~ purge) {
            return(synth(405, "Method Not Allowed"));
        }
        return (purge);
    }

    new plone = directors.round_robin();
    plone.add_backend(instance1);
    plone.add_backend(instance2);
}

sub vcl_recv {
    set req.backend_hint = plone.backend();
    call sanitize_cookies;
    call sanitize_url_parameters;
}

sub vcl_hit {
    if (obj.ttl >= 0s) {
        return (deliver);
    }

    if (!std.healthy(req.backend_hint) && (obj.ttl + obj.grace > 0s)) {
        return (deliver);
    }

    return (fetch);
}

sub vcl_backend_response {
    set beresp.grace = 1h;
}

sub vcl_deliver {
    unset resp.http.X-UA-Compatible;
}

# clean up all cookies but the ones we should care
# https://www.varnish-cache.org/docs/4.0/users-guide/increasing-your-hitrate.html#cookies
# http://docs.plone.org/develop/plone/sessions/cookies.html
sub sanitize_cookies {
    if (req.http.Cookie) {
        set req.http.Cookie = ";" + req.http.Cookie;
        set req.http.Cookie = regsuball(req.http.Cookie, "; +", ";");
        set req.http.Cookie = regsuball(req.http.Cookie, ";(__ac|__cp|_ZopeId|statusmessages)=", "; \1=");
        set req.http.Cookie = regsuball(req.http.Cookie, ";[^ ][^;]*", "");
        set req.http.Cookie = regsuball(req.http.Cookie, "^[; ]+|[; ]+$", "");

        if (req.http.Cookie == "") {
            unset req.http.Cookie;
        }
    }
}

# remove Google Analytics parameters from the URL
# https://github.com/mattiasgeniar/varnish-4.0-configuration-templates
sub sanitize_url_parameters {
    if (req.url ~ "(\?|&)(utm_campaign|utm_content|utm_medium|utm_source|utm_term)=") {
        set req.url = regsuball(req.url, "&(utm_campaign|utm_content|utm_medium|utm_source|utm_term)=([A-z0-9_\-\.%25]+)", "");
        set req.url = regsuball(req.url, "\?(utm_campaign|utm_content|utm_medium|utm_source|utm_term)=([A-z0-9_\-\.%25]+)", "?");
        set req.url = regsub(req.url, "(\?&|\?|&)$", "");
    }
}
