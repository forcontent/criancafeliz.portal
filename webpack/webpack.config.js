var SpritesmithPlugin = require('webpack-spritesmith');
module.exports = {
  entry: [
    './app/index.html',
    './app/style.less',
    './app/img/gov-logo-mini.png',
    './app/img/preview.png',
    './app/img/topo.png',
    './app/behavior.js'
  ],
  output: {
    filename: 'js/behavior.js',
    path: __dirname + '/../src/criancafeliz/portal/theme',
    libraryTarget: 'umd',
    publicPath: '/++theme++Default/',
    library: 'criancafeliz'
  },
  module: {
    rules: [{
      test: /\.html$/,
      use: [
        {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
          }
        },
        'extract-loader',
        {
          loader: 'html-loader',
          options: {
            minimize: false
          }
        },
      ]
    }, {
      test: /\.js$/,
      exclude: /(\/node_modules\/|test\.js$|\.spec\.js$)/,
      use: 'babel-loader',
    }, {
      test: /\.less$/,
      use: [
        {
          loader: 'file-loader',
          options: {
            name: 'css/[name].css',
          }
        },
        'extract-loader',
        'css-loader',
        'postcss-loader',
        'less-loader'
      ]
    }, {
      test: /.*\.(gif|png|jpe?g)$/i,
      use: [
        {
          loader: 'file-loader',
          options: {
            name: '[path][name].[ext]',
            context: 'app/'
          }
        },
        {
          loader: 'image-webpack-loader',
          query: {
            mozjpeg: {
              progressive: true,
            },
            pngquant: {
              quality: '65-90',
              speed: 4
            },
            gifsicle: {
              interlaced: false
            },
            optipng: {
              optimizationLevel: 7
            }
          }
        }
      ]
    }, {
      test: /\.svg/,
      exclude: /node_modules/,
      use: 'svg-url-loader'
    }]
  },
  devtool: 'source-map',
  plugins: [
    new SpritesmithPlugin({
      src: {
        cwd: 'app/sprite',
        glob: '*.png'
      },
      target: {
        image: 'app/img/sprite.png',
        css: 'app/less/sprite.less'
      },
      apiOptions: {
        cssImageRef: '../img/sprite.png'
      }
    })
  ]
}
