import Shuffle from 'shufflejs/dist/shuffle.js';


let instance = null;


export default class VideoSearch {
  constructor() {
    if (instance != null) {
      return instance;
    }
    instance = this;
    this.$el = $('.videosearch');
    this.paginationSize = 6;
    this.allowScroll = false;
    this.waitTime = 500;
    this.timeoutID = null;
    this.currentSearch = '';
    this.total = $('.videosearch-item').length;
    this.visible = 0;
    this.shuffle = new Shuffle($('.videosearch-results')[0], {
      itemSelector: '.videosearch-item',
      sizer: '.sizer-element'
    });

    if ($('.section-pagina-inicial').length > 0) {
      this.allowScroll = true;
      this.visible += this.paginationSize;
    }
    $('.videosearch-item').attr('data-order', '');
    this.update();
    this.bindEvents();
    return instance;
  }

  update(order) {
    if (order == null) {
      order = 0;
    }
    // Fill data-order attribute for elements without it
    $('.videosearch-item[data-order=""]').each(function() {
      order++;
      $(this).attr('data-order', order);
    });
    // filter and sort at same time
    this.shuffle.filter(function(el, shuffle) {
      return parseInt($(el).attr('data-order')) <= this.visible;
    }.bind(this), {
      by: function(el) {
        return parseInt($(el).attr('data-order'));
      }
    });
  }

  bindEvents() {
    $(window).on('scroll', this.scrollEvent.bind(this));
    $('.videosearch-input > input').on('keyup', this.keyUp.bind(this));
  }

  scrollEvent(e) {
    e.preventDefault();

    if (this.allowScroll === false) {
      return;
    }
    if ($(window).scrollTop() !== $(document).height() - $(window).height()) {
      return;
    }
    if (this.visible === this.total) {
      return;
    }

    this.visible += this.paginationSize;
    this.update();
  }

  keyUp() {
    let searcheableText = $('.videosearch-input input').val();
    if (searcheableText.length === 0) {
      this.allowScroll = false;
      this.visible = 0;
      if ($('.section-pagina-inicial').length > 0) {
        this.allowScroll = true;
        this.visible += this.paginationSize;
      }
      $('.videosearch-item').attr('data-order', '');
      this.update();
    }
    if (searcheableText.length <= 3) {
      return;
    }
    if (searcheableText === this.currentSearch) {
      return;
    }
    this.currentSearch = searcheableText;

    // Clear old timeout
    if (this.timeoutID != null) {
      clearTimeout(this.timeoutID);
    }

    // Create new timeout
    this.timeoutID = setTimeout(
      $.proxy(this.doSearch, this),
      this.waitTime
    );
  }

  doSearch() {
    let searcheableText = $('.videosearch-input input').val();
    $.ajax({
      headers: {
        Accept: 'application/json'
      },
      url: portal_url + '/@search',
      data: {
        SearchableText: searcheableText,
        portal_type: 'sc.embedder',
        review_state: 'published'
      },
      context: this
    }).done(function(result) {
      if (result.items.length === 0) {
        return;
      }
      $('html, body').animate({
          scrollTop: $('.videosearch').offset().top
      }, 500);
      $('.videosearch-item').attr('data-order', '');
      this.allowScroll = true;
      if (this.visible === 0) {
        this.visible += this.paginationSize;
      }
      let order = 0;
      result.items.forEach(function(item) {
        order++;
        let id = item['@id'].split('/').pop();
        let $el = $('.videosearch-item[data-id='+id+']');
        $el.attr('data-order', order);
      });
      this.update(order);
    });
  }
}
