import Youtube from './youtube.js';

let instance = null;
export default class Cover {
  constructor() {
    if (instance != null) {
      return instance;
    }
    instance = this;
    this.iframeReady = false;
    this.openWhenReady = false;
    this.$imagemCapa = $('.imagem-capa > img');
    this.$bemVindo = $('.bem-vindo > a');
    this.$assistaAgora = $('.assista-agora > img');

    $(window).on('orientationchange', this.orientationChange.bind(this));
    this.$bemVindo.on('click', this.bemVindo.bind(this));
    this.$assistaAgora.on('click', this.assistaAgora.bind(this));
  }
  iframeIsReady() {
    this.iframeReady = true;
    if (this.openWhenReady) {
      this.openVideo();
    }
    $('body').append(
      `<div class="video-capa">
        <div id="player"></div>
       </div>`
    );
    this.cfy = new Youtube();
    this.cfy.height = this.$imagemCapa.height();
    this.cfy.width = this.$imagemCapa.width();
    this.cfy.videoId = this.cfy.extractId(this.$bemVindo.attr('href'));
    this.cfy.bindEvents();
  }
  orientationChange(e) {
    if (this.cfy.playerState === 'paused') {
      return;
    }
    $('#main').css({height: $('.video-capa').height() - 6});
  }
  openVideo() {
    $('.video-capa').show(500, function(e) {
      $('.bem-vindo, .imagem-capa, .assista-agora').hide(500);
      $('#main').css({height: $('.video-capa').height() - 6});
    }.bind(this));
    this.cfy.player.playVideo();
  }
  bemVindo(e) {
    e.preventDefault();
    if (this.iframeReady === false) {
      this.openWhenReady = true;
    } else {
      this.openVideo();
    }
  }
  assistaAgora(e) {
    e.preventDefault();
    $('html, body').animate({
        scrollTop: $('.videosearch').offset().top
    }, 500);
  }
}
