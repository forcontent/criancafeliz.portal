let instance = null;

export default class Embedder {
  constructor() {
    if (instance != null) {
      return instance;
    }
    instance = this;
    this.stopSocialLikeAnimation();
    this.colorTitle();
  }
  stopSocialLikeAnimation() {
    // Remove social like animation
    $('#viewlet-social-like').stop(true, true);
  }
  colorTitle() {
    // Add pink class to title
    let $titleEl = $('.documentFirstHeading');
    let titleSplit = $titleEl.text().split(' ');
    $titleEl.html(`${titleSplit.slice(0, 2).join(' ')} <span class="pink">${titleSplit.slice(2).join(' ')}</span>`);
  }
}
