let instance = null;


export default class Youtube {
  constructor() {
    if (instance != null) {
      return instance;
    }
    instance = this;
    this.$logo = $('#logo');
    this.percentToHide = 55;
    this.playerState = 'paused';
  }
  fixEmbedded() {
    if ($('.template-view.portaltype-sc-embedder').length === 0) {
      return;
    }
    let $currentVideo = $('.top_embedded iframe');

    this.height = $currentVideo.attr('height');
    this.width = $currentVideo.attr('width');
    this.videoId = this.extractId($currentVideo.attr('src'));

    $currentVideo.before('<div id="player"></div>');
    $currentVideo.remove();
  }
  bindEvents() {
    var playerVars = {
      rel: 0,
      controls: 0,
      showinfo: 0,
      autoplay: 1
    };
    if ($('.template-view.portaltype-sc-embedder').length === 0) {
      playerVars.autoplay = 0;
    }
    this.player = new YT.Player('player', {
      height: this.height,
      width: this.width,
      videoId: this.videoId,
      playerVars: playerVars,
      events: {
        onStateChange: this.playerStateChange.bind(this)
      }
    });
    this.$logo.hover(
      this.logoHoverIn.bind(this),
      this.logoHoverOut.bind(this)
    );
  }
  extractId(url) {
    let parser = document.createElement('a');
    parser.href = url;
    return parser.pathname.split('/')[2];
  }
  hideHeader() {
    this.$logo.stop(true, true).animate({
      top: -1 * (this.$logo.height() * this.percentToHide / 100)
    }, 500);
  }
  showHeader() {
    this.$logo.stop(true, true).animate({top: 0}, 500);
  }
  playerStateChange(e) {
    if (e.data === YT.PlayerState.PLAYING || e.data === YT.PlayerState.BUFFERING) {
      this.playerState = 'playing';
      this.hideHeader();
    } else {
      this.playerState = 'paused';
      this.showHeader()
    }
  }
  logoHoverIn(e) {
    if (this.playerState === 'paused') {
      return;
    }
    this.showHeader();
  }
  logoHoverOut(e) {
    if (this.playerState === 'paused') {
      return;
    }
    this.hideHeader();
  }
}
