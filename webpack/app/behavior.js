import Cover from './js/cover.js';
import Embedder from './js/embedder.js';
import Youtube from './js/youtube.js';
import VideoSearch from './js/videosearch.js';


$(() => {
  if ($('.section-pagina-inicial').length > 0) {
    new Cover();
  }
  if ($('.template-view.portaltype-sc-embedder').length > 0) {
    new Embedder();
  }
  new VideoSearch();
});


module.exports = {
  Cover,
  Embedder,
  Youtube,
  VideoSearch
}
