# -*- coding: utf-8 -*-
from setuptools import find_packages
from setuptools import setup

version = '1.0'
description = 'Portal Vida da Criança'
long_description = (
    open('README.rst').read() + '\n' +
    open('CONTRIBUTORS.rst').read() + '\n' +
    open('CHANGES.rst').read()
)

setup(
    name='criancafeliz.portal',
    version=version,
    description=description,
    long_description=long_description,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Framework :: Plone',
        'Framework :: Plone :: 4.3',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: GNU General Public License v2 (GPLv2)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    keywords='criancafeliz',
    author='forContent',
    author_email='suporte@forcontent.com.br',
    url='https://bitbucket.org/forcontent/criancafeliz.portal.git',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    namespace_packages=['criancafeliz'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'collective.cover',
        'collective.fingerpointing',
        'collective.lazysizes',
        'collective.revisionmanager',
        'collective.upload',
        'plone.app.caching',
        'plone.app.contenttypes',
        'plone.app.theming',
        'plone.autoform',
        'plone.resource',
        'plone.restapi',
        'Products.GenericSetup',
        'Products.PloneFormGen',
        'Products.PloneKeywordManager',
        'Products.RedirectionTool',
        'sc.embedder',
        'sc.social.like',
        'setuptools',
        'z3c.jbot',
        'zope.interface',
    ],
    extras_require={
        'test': [
            'plone.app.testing',
            'plone.browserlayer',
            'plone.testing',
            'plone.app.robotframework',
        ],
    },
    entry_points="""
      [z3c.autoinclude.plugin]
      target = plone
      """,
)
