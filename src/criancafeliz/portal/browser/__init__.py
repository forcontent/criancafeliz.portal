# -*- coding: utf-8 -*-
from plone import api
from plone.app.layout.viewlets import ViewletBase
from Products.Five.browser import BrowserView
from sc.embedder.content.embedder import IEmbedder


class ExternalRelatedItems(ViewletBase):
    def has_items(self):
        return bool(self.context.external_related_items)

    def external_related(self):
        for item in self.context.external_related_items:
            k, v = item.split('|')
            yield {'title': k.strip(), 'url': v.strip()}


class VideosSearchView(BrowserView):

    def videos(self):
        return api.content.find(object_provides=IEmbedder, review_state='published')
