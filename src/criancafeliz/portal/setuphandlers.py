# -*- coding: utf-8 -*-
from Products.CMFPlone.interfaces import INonInstallable
from Products.CMFQuickInstallerTool.interfaces import INonInstallable as INonInstallableProducts
from zope.interface import implementer


@implementer(INonInstallableProducts)
class NonInstallableProducts(object):

    def getNonInstallableProducts(self):  # pragma: no cover
        return [
            'collective.z3cform.datetimewidget',
            'plone.app.blocks',
            'plone.app.dexterity',
            'plone.app.event',
            'plone.app.event.at',
            'plone.app.tiles',
            'plone.formwidget.autocomplete',
            'plone.formwidget.contenttree',
            'plone.formwidget.datetime',
            'plone.formwidget.querystring',
            'plone.formwidget.recurrence',
            'Products.Marshall',
        ]


@implementer(INonInstallable)
class NonInstallable(object):

    def getNonInstallableProfiles(self):  # pragma: no cover
        """Do not show on Plone's list of installable profiles."""
        return [
            u'collective.cover:default',
            u'collective.fingerpointing:default',
            u'collective.js.galleria:default',
            u'collective.js.jqueryui:default',
            u'collective.lazysizes:default',
            u'collective.revisionmanager:default',
            u'collective.upload:default',
            u'plone.app.blocks:default',
            u'plone.app.caching:default',
            u'plone.app.contenttypes:plone-content',
            u'plone.app.dexterity:default',
            u'plone.app.event.at:default',
            u'plone.app.event:default',
            u'plone.app.iterate:plone.app.iterate',
            u'plone.app.relationfield:default',
            u'plone.app.theming:default',
            u'plone.app.tiles:default',
            u'plone.app.versioningbehavior:default',
            u'plone.formwidget.autocomplete:default',
            u'plone.formwidget.contenttree:default',
            u'plone.formwidget.querystring:default',
            u'plone.formwidget.recurrence:default',
            u'plone.restapi:default',
            u'plone.restapi:performance',
            u'plone.session:default',
            u'Products.CMFPlacefulWorkflow:base',
            u'Products.PloneFormGen:default',
            u'Products.PloneKeywordManager:default',
            u'Products.PloneKeywordManager:uninstall',
            u'Products.RedirectionTool:default',
            u'sc.embedder:default',
            u'sc.social.like:default',
        ]


def post_install(context):
    """Post install script."""
