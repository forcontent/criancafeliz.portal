# coding: utf-8
from criancafeliz.portal import _
from criancafeliz.portal.utils import validate_external_related_items
from plone.autoform import directives as form
from plone.autoform.interfaces import IFormFieldProvider
from plone.supermodel import model
from zope import schema
from zope.interface import provider


@provider(IFormFieldProvider)
class IExternalRelatedItems(model.Schema):

    """Behavior interface to add External Related Items."""

    model.fieldset(
        'external-related-items',
        label=_(u'External Related Items'),
        fields=['external_related_items'],
    )

    form.widget('external_related_items', cols=25, rows=10)
    external_related_items = schema.Tuple(
        title=_(u'External Related Items'),
        required=False,
        default=(),
        value_type=schema.TextLine(),
        constraint=validate_external_related_items,
    )
