# -*- coding: utf-8 -*-
from criancafeliz.portal import _
from urlparse import urlparse
from zope.interface import Invalid


MESSAGE = _(u'You must use "Title|http://example.org" format to fill each line.')


def validate_external_related_items(value):
    """Validate External Related Items behavior items."""
    if not value:
        return True
    for item in value:
        # Check string format
        if '|' not in item or item.count('|') > 1:
            raise Invalid(MESSAGE)

        # Check if url is valid
        _, v = item.split('|')
        parsed = urlparse(v.strip())
        if not all([parsed.scheme, parsed.netloc]):
            raise Invalid(MESSAGE)
    return True
