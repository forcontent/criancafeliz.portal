# -*- coding: utf-8 -*-
from criancafeliz.portal.testing import INTEGRATION_TESTING
from zope.component import getUtility
from zope.schema.interfaces import IVocabularyFactory

import unittest


class SitePolicyTestCase(unittest.TestCase):

    layer = INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']

        vocab = getUtility(
            IVocabularyFactory, 'collective.cover.EnabledTiles')
        self.enabled_tiles = vocab(self.portal)

    def test_embedder_tile_enabled(self):
        self.assertIn(u'sc.embedder', self.enabled_tiles)
