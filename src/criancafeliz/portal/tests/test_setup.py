# -*- coding: utf-8 -*-
from criancafeliz.portal.config import PROJECTNAME
from criancafeliz.portal.interfaces import IBrowserLayer
from criancafeliz.portal.testing import INTEGRATION_TESTING
from plone.browserlayer.utils import registered_layers

import unittest


DEPENDENCIES = (
    'collective.cover',
    'collective.fingerpointing',
    'collective.lazysizes',
    'collective.upload',
    'plone.app.caching',
    'plone.app.theming',
    'PloneFormGen',
    'PloneKeywordManager',
    'sc.embedder',
    'sc.social.like',
)


class InstallTestCase(unittest.TestCase):
    """Ensure product is properly installed."""

    layer = INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.qi = self.portal['portal_quickinstaller']

    def test_installed(self):
        self.assertTrue(self.qi.isProductInstalled(PROJECTNAME))

    def test_dependencies_installed(self):
        expected = set(DEPENDENCIES)
        installed = self.qi.listInstalledProducts(showHidden=True)
        installed = set([product['id'] for product in installed])
        result = sorted(expected - installed)
        self.assertEqual(result, [], 'Not installed: ' + ', '.join(result))

    def test_browser_layer_installed(self):
        self.assertIn(IBrowserLayer, registered_layers())
